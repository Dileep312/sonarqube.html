node {
    try {
        stage('Ang-SonarQube-Analysis'){
            def DevServerIp = '18.208.226.137'
            def JenkinsHome = "/home/centos"
            def BuildStart = "${JenkinsHome}/npm.sh"
            sshagent (credentials: ['TestCentos']) {
                sh "ssh -o StrictHostKeyChecking=no centos@${DevServerIp} ${BuildStart}"
            }
        }      
    }
}
